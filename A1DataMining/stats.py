import merge
import string
import numpy as np
import matplotlib.pyplot as plt

# Stats variables (cumulate per dialogs)
nb_speakers = 0
nb_utterances = 0
nb_words = 0
nb_words_per_utt = 0
nb_dialog = 0

# Stats per dialog
utterances = []
speakers = []
words_per_utterances = []

# taken from https://stackoverflow.com/questions/23175809/typeerror-translate-takes-one-argument-2-given-python
trans_table = {ord(c): None for c in string.punctuation}

# Generate stats for one dialog
def generate_dialog_stats(dialog):
    global nb_speakers
    global nb_utterances
    global nb_words
    global nb_words_per_utt

    uids = set()
    words = 0
    words_in_diag = 0

    for utt in dialog:
        # Add uid to set
        uids.add(utt['id'])

        # Count stats for the utterance
        words = len(utt['utt'].translate(trans_table).split())
        words_in_diag += words


    utt_in_diag = len(dialog)
    # Count stats for the dialog
    nb_speakers += len(uids)
    nb_utterances += utt_in_diag
    nb_words += words_in_diag
    nb_words_per_utt += words_in_diag/utt_in_diag

    utterances.append(utt_in_diag)
    speakers.append(len(uids))
    words_per_utterances.append(words_in_diag/utt_in_diag)

# Generate stats for all dialogs
def generate_stats(dialogs):
    
    for dialog in dialogs:
        generate_dialog_stats(dialog)

    # Count total number of dialogs
    nb_dialog = len(dialogs)
    
    # Print stats
    print "Number of dialogs : ", nb_dialog
    print "Number of words : ", nb_words
    print "Number of words per dialog : ", nb_words / nb_dialog
    print "Number of words per utterances : ", nb_words_per_utt / nb_dialog
    print "Number of utterances : ", nb_utterances
    print "Number of utternaces per dialog : ", nb_utterances / nb_dialog
    print "Number of speakers per dialog : ", nb_speakers / nb_dialog


dialogs = []
merge.get_dialogs_from_file(merge.outputxmlfile, dialogs)

generate_stats(dialogs)

# Speakers per utterance
n, bins, patches = plt.hist(speakers, 50, facecolor='green', alpha=0.75)

plt.xlabel('Nb. of Speakers in a Dialog', fontsize = 14)
plt.ylabel('Frequency', fontsize = 14)
plt.title('Distribution of Speakers in Dialogs', fontsize=16)
plt.grid(True)

plt.show()



# Utterances per dialog
n, bins, patches = plt.hist(utterances, 50, facecolor='green', alpha=0.75)

plt.xlabel('Nb. of Utterances in a Dialog', fontsize = 14)
plt.ylabel('Frequency', fontsize = 14)
plt.title('Distribution of Utterances in Dialogs', fontsize = 16)
plt.grid(True)

plt.show()


# Words per utterance
n, bins, patches = plt.hist(words_per_utterances, 50, facecolor='green', alpha=0.75)

plt.xlabel('Words per Utterance in a Dialog', fontsize=14)
plt.ylabel('Frequency', fontsize=14)
plt.title('Distribution of Words per Utterance', fontsize = 16)
plt.grid(True)

plt.show()



