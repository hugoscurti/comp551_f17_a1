import os
import sys
import codecs
import re
import random
from lxml import etree

# User defined import
from qc_national_assembly import export_dataset as exp

reload(sys)
sys.setdefaultencoding('UTF-8')

xmlrootpath = "./xml_dialogs"
xmlfiles = [
    "hugo_fre.xml",
    "celly_fre_1.xml",
    "celly_fre_2.xml",
    "paul_fre.xml",
]

# File that contains all xml files combined
outputxmlfile = "./xml_dialogs/groupname_fre.xml"

# Retreive dialog from an xml tag <s></s>
def get_dialog(element):

    if element.tag != "s":
        raise RuntimeError("dialog tag must be s")
   
    dialog = []
    for utt in element:
        if utt.tag != "utt":
            raise RuntimeError("utterance tag must be utt")
        uid = utt.get("uid")
        if uid is None:
            raise RuntimeError("uid is not set")

        text = utt.text

        #Check for empty utterance
        if not text or text.isspace():
            continue

        # Remove these \xa0 characters (html's non-breakable space?)
        text = text.replace(u'\xa0', u' ')
        # Do stuff here

        dialog.append({'id': uid,
                       'utt': text})

    #Check for empty dialogs?
    if len(dialog) == 0:
        return None

    return dialog



def get_dialogs_from_file(file, dialogs):
    elmtree = etree.parse(file)
    xmldialog = elmtree.getroot()
    # do something with it
    if xmldialog.tag == 'dialog':
        for d in xmldialog:
            dialog = get_dialog(d)
            if dialog is not None:
                dialogs.append(dialog)

def combine_xml_files(files):
    
    dialogs = []
    
    for xmlfile in files:
        get_dialogs_from_file(os.path.join(xmlrootpath, xmlfile), dialogs)

    # Shuffle in place
    random.shuffle(dialogs)

    # Write to xml file
    exp.write_to_xml(dialogs, outputxmlfile)

    return dialogs

if __name__ == '__main__':
    dialogs = combine_xml_files(xmlfiles)
    



