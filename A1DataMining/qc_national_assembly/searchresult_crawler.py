from bs4 import BeautifulSoup
import re
import codecs
import unicodedata
import os

def get_link(filename, all_links, server_url):
    """
        Function that goes through an html file and cumulates all link
        of a specific format into the list all_links.
    """

    with codecs.open(filename, 'rb', 'utf-8') as fp:
        soup = BeautifulSoup(unicodedata.normalize('NFKD', fp.read()), 'lxml')

    #table = soup.body.find('table', id='tblListeJournal')
    links = soup.body.select('table#tblListeJournal td.colonneDate a')

    for link in links:
        if server_url not in link['href']:
            href = server_url + link['href']
        else:
            href = link['href']
        
        all_links.append(href)
    
def get_links(folder, server_url):
    """
        Function that gets all links of a specific format in search result html files from a specific folder.
    """

    all_links = []

    for file in os.listdir(folder):
        get_link(os.path.join(folder, file), all_links, server_url)

    return all_links


