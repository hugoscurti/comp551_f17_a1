COMP551-F17 Project 1
=====
Contributors :

- Hugo Scurti
- Marcellina Indrawan Daniel
- Paul Thulstrup

- - -

Overview
---
This repository contains python code used to extract dialogs from the National Assembly of France ([website](http://www.assemblee-nationale.fr/)) and the National Assembly of Quebec ([website](http://www.assnat.qc.ca/fr/abc-assemblee/assemblee-nationale/index.html)). 

The repository is divided into 2 folders : 

- french_assembly : contains the code used to extract dialogs from the National Assembly of France
- qc_national_assembly : contains the code used to extract dialogs from the National Assembly of Quebec and parts of the code to export a python dialog list into a specific xml format.

- - -

Note : The dialogs extracted from those websites are solely used for educational purposes. 