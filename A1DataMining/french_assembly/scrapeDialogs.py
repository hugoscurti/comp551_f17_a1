import requests
import re
import fnmatch
from bs4 import BeautifulSoup
import codecs
    
# Code to scrape the dialogs from French National Assembly from 2012 to 2014

def readFromSite(link):
    page = requests.get(link)
    soup = BeautifulSoup(page.content, 'html.parser')
    return soup
    
def cleanCommentary(soup, tag):
    for comment in soup.find_all(tag):
        comment.replaceWith('')
    return soup
    
def getUserList(soup, tag):
    userNames = soup.find_all(tag)
    uid = {}
    for userName in userNames:
        name = userName.getText()
        if name not in uid: 
            uid[name] = len(uidDict)+1
    return uid
    
def getContent(soup, tag):
    return soup.find_all(tag)
    
    
pageLink = 'http://www.assemblee-nationale.fr/14/cri/2016-2017/20170121.asp'
print getContent(readFromSite(pageLink), 'div', {'class' : 'intervention'} )