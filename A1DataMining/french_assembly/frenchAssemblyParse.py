# coding=utf-8
# Import libraries
from bs4 import BeautifulSoup
import urllib
import codecs
import re

def parseToDialog(convo):
    """
    Function to transform a List corresponding to one conversation of tags into a List of utterances with anonymized users

    :param convo: List containing all the tag of the conversation
    :return: List of the utterances of one conversation
             Null if the conversation contains less than 3 utterances
    """

    parsedConvo = []

    dict_uid = {}
    inUtterance = False
    user_count = 1
    utt = ""



    for line in convo:
        # Create BeautifulSoup Object
        for tag in line.find_all('i'):
            tag.replaceWith('')  # Remove all italic (only correspond to title and comments)

        b_tag = line.find('b')
        if not b_tag is None:

            if inUtterance:
                utt = utt + "</utt>"
                matchEmptyUtt = re.search(r'<utt uid="\d"></utt>', utt)
                if not matchEmptyUtt:
                    parsedConvo.append(utt)


            # Check if user is <a>
            a_tag = b_tag.find('a')
            if not a_tag is None:
                user = str(a_tag.contents)
            else:
                user = str(b_tag.contents)

            # Anonymize who said
            if user in dict_uid:
                user_id = dict_uid[user]

            else:
                user_id = user_count
                dict_uid[user] = user_count
                user_count += 1

            utt = '<utt uid="' + str(user_id) + '">'
            inUtterance = True

        for tag in line.find_all('b'):
            tag.replaceWith('')  # Remove all italic (only correspond to title and comments)

        utt = utt + line.text


    utt = utt + "</utt>"
    matchEmptyUtt = re.search(r'<utt uid="\d"></utt>', utt)
    if not matchEmptyUtt:
        parsedConvo.append(utt)

    return parsedConvo


utterance_count = 0
conversation_count = 0
conversations = []
with open("Links_that_are_not_scraped.txt") as f:
    urls = f.readlines()

with codecs.open("dialog_paul.xml", 'w', 'utf-8') as w:
    w.write("<dialog>\n")  # Opening tag

    for url in urls:
        print url
        # Open link
        r = urllib.urlopen(url).read()
        soup = BeautifulSoup(r, "html.parser")

        # Select the beginning of the session
        start = soup.select("h5")[0]

        temp = []
        counter = 0

        conversations = []

        # Go through all of the following siblings
        for nextSibling in start.next_siblings:

            if nextSibling.name == "h2" or nextSibling.name == "h5":
                # We have a new theme with new conversation
                if len(temp) > 2:
                    # if the conversation add more than two utterances, we keep it as one conversation
                    conversations.append(temp)
                    counter += 1

                temp = []  # Clear

            if nextSibling.name == "p":
                temp.append(nextSibling)  # Adding the utterance in the conversation list

        for conversation in conversations:
            result = parseToDialog(conversation)  # Obtaining the parsed utterances of the conversation

            if len(result) > 3:
                w.write("<s>")  # Open a conversation
                # Write all of the utterance in the file
                for utterance in result:
                    w.write(utterance)
                    utterance_count += 1
                w.write("</s>\n")  # Close a conversation
                conversation_count += 1


    w.write("</dialog>\n")  # Closing tag

print utterance_count
print conversation_count
