import cPickle as pickle
import codecs
import page_crawler as pg
import os
import sys
import re

# Usefull function that takes an array of dialog and export it in xml format to the specified file
def write_to_xml(dialogs, filename):

    uttformat = '<utt uid="{0}">{1}</utt>'

    print 'Dumping dialogs into xml format...'
    with codecs.open(filename, 'w', 'utf-8') as f:
        f.write("<dialog>\n")
        for dialog in dialogs:
            f.write("    <s>")

            for utt in dialog:
                # Replace ampersand by xml code
                # remoce tags characters
                f.write(uttformat.format(utt['id'], utt['utt'].replace('&', '&amp;').replace('<','').replace('>','')))

            f.write("</s>\n")
        f.write("</dialog>")

    print 'Finished dumping dialogs in xml file.'

def collect_dumps():
    # Open the mapping file
    if not os.path.isfile(pg.link_mapping_file):
        print 'Cannot export dataset since mappings are not found'
        exit()

    with open(pg.link_mapping_file, 'rb') as f:
        link_mapping = pickle.load(f)

    # Array containing all dialogs
    dialogs = []
    ids = link_mapping['ids']

    for id in ids.values():
        filename = os.path.join(pg.result_dump_folder, pg.dialog_filename.format(id))
        if os.path.isfile(filename):
            with open(filename, 'rb') as f:
                dialogs.extend(pickle.load(f))

    return dialogs

def mainfct():
    # Check for all_diags file
    if os.path.isfile(pg.all_dialogs_file):
        with open(pg.all_dialogs_file, 'rb') as f:
            dialogs = pickle.load(f)

    else:
        dialogs = collect_dumps()
        all_diags_dump = os.path.join(pg.result_dump_folder, 'all_diags')
        # Collect and dump them in one file
        print 'Dumping all dialogs...'
        with open(all_diags_dump, 'wb') as f:
            pickle.dump(dialogs, f)
        print 'Finished dumping dialogs.'


    print 'Nb dialogs:', len(dialogs)

    nbutt = 0
    # Count the number of utterances
    for dialog in dialogs:
        nbutt += len(dialog)

    print 'Nb utterances:', nbutt

    write_to_xml(dialogs, './xml_dialogs/hugo_fre.xml')


if __name__ == '__main__':
    reload(sys)
    sys.setdefaultencoding('UTF-8')

    mainfct()



