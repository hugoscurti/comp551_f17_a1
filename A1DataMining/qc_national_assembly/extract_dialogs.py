from bs4 import BeautifulSoup
import bs4
import re
import codecs
import unicodedata

# Regex used in parsing content
re_center_p = re.compile('text\-align\s*\:\s*center')
re_speaker = re.compile('^\s*(\S.*\S)\s*\:\s*$', re.DOTALL)
re_voices = re.compile('^\s*(?:Une|Des)\s*voix\s*\:\s*$')
re_parenthese_tag = re.compile('^\W*\s*\((.*)\)\s*\W*$', re.DOTALL)
re_suspension = re.compile('(?:[Ss]uspension|[Ff]in)')

# Used to format utterance
re_spaces = re.compile('\s{2,}', re.DOTALL)
re_parentheses = re.compile('\(.*?\)')

def EndUtterance(current_speaker, utterance, dialog):
    """ Process the end of an utterance and
    insert it into the current dialog if it's not empty
    """

    if current_speaker > 0:
        utterance = utterance.replace('\n', ' ').replace(u'\xa0', ' ')
        utterance = utterance.strip()
        utterance = re_spaces.sub(' ', utterance)
        utterance = re_parentheses.sub('', utterance)

        if (utterance and not utterance.isspace()):
            dialog.append({'id': current_speaker, 'utt': utterance})
        else:
            pass
        
        current_speaker = 0
        utterance = ''

    return current_speaker, utterance

def EndDialog(dialog, dialogs, speakers):
    """ Process the end of the current dialog and 
    insert it into the dialogs list
    """

    if len(dialog) > 1:
        # End conversation
        dialogs.append(dialog)
    else:
        pass

    return [], {}, 1

def ExtractSpeakerId(speaker, ids, nextId):
    """ Get the speaker id if it already exists in the dictionary.
    Otherwise add it to the dictionary with a new id
    """

    #Format speaker name before checking into dict
    speaker = speaker.replace('\n', ' ').replace(u'\xa0', ' ').strip()
    speaker = re_spaces.sub(' ', speaker)

    speaker_id = ids.get(speaker)

    if speaker_id is None:
        #We add the speaker to the dictionary
        speaker_id = ids.setdefault(speaker, nextId)
        nextId += 1

    return speaker_id, nextId


def GetBoldText(p, tagname):
    """ Get consecutive bold text in a specific tag name (<b> or <strong>)
    and concatenate it into a string
    """

    # Check if there is multiple boldtexts close to each other
    previous_sibling = p.find(tagname)
    boldtext = previous_sibling.get_text()
    sibling = previous_sibling.next_sibling
    previous_sibling.decompose()

    while sibling is not None and (sibling.name == tagname or (type(sibling) is bs4.element.NavigableString and sibling.string.isspace())):
        if sibling.name == tagname:
            boldtext += sibling.get_text()
        else:
            boldtext += ' '

        previous_sibling = sibling
        sibling = sibling.next_sibling
        if type(previous_sibling) is not bs4.element.NavigableString:
            previous_sibling.decompose()

    return sibling, boldtext


def ProcessParagraphs(paragraphs):
    """ Process all paragraphs passed from the list and
    retreive the dialogs from them.
    """

    dialogs = []
    dialog = []
    speakers = {}

    nextId = 1
    current_speaker = 0
    utterance = ''

    # Iterate through paragraphs
    for p in paragraphs:
        if p.has_attr('style') and re_center_p.search(p['style']):
            # Every text-align: center represents a sub title which is not needed in the conversation
            continue

        # Removes <u> tags because it's dupplicated dialogs
        duppls = p.find_all('u')
        for duppl in duppls:
            duppl.decompose()
        
        # Check for italic parentheses
        if p.i is not None or p.em is not None:
            parenthmatch = re_parenthese_tag.match(p.get_text(strip=True))
            if parenthmatch is not None:
                if re_suspension.search(parenthmatch.group(1)):
                    
                    # End utterance if not ended
                    current_speaker, utterance = EndUtterance(current_speaker, utterance, dialog)
                    # End dialog
                    dialog, speakers, nextId = EndDialog(dialog, dialogs, speakers)
                
                # Since parentheses are always alone in a p, we continue onto the next one    
                continue
                

        # Check if there is still something to do after those operations
        if not p.get_text(strip=True):
            continue

        if p.b is not None or p.strong is not None:

            if p.b is not None and p.strong is not None:
                raise RuntimeError("Found b and strong in same paragraphs...")

            if p.b is not None:
                sibling, boldtext = GetBoldText(p, 'b')
            else:
                sibling, boldtext = GetBoldText(p, 'strong')


            if re_voices.match(boldtext):
                # We don't care about voices in the room
                continue

            speakermatch = re_speaker.match(boldtext)
            if speakermatch is not None:
                # End the previous speaker utterance
                current_speaker, utterance = EndUtterance(current_speaker, utterance, dialog)
                
                # Start a new utterance
                current_speaker, nextId = ExtractSpeakerId(speakermatch.group(1), speakers, nextId)
            else:
                # We must put back the text we removed
                if sibling is not None: 
                    if type(sibling) is bs4.element.NavigableString:
                        sibling.replace_with(boldtext + sibling.string)
                    else:
                        sibling.insert(0, boldtext)

        #Extract dialog
        text = p.get_text(strip=True)
        if text :
            utterance += ' ' + text

    # Finally, return the dialogs extracted
    return dialogs




def __ExtractDialogs__(soup):
    """ Internal method for extracting dialogs. Takes a soup object in parameter
    """

    content = soup.find('contenu')
    paragraphs = []

    # Make sure that contenu tag exists in page
    if content is not None:
        paragraphs = content.find_all('p', recursive=False)
    else:

        # Revision mode, we have multiple divs with no class which contain the paragraphs
        content = soup.select("h2 a[name='debut_journal']")

        if len(content) < 1:
            raise RuntimeError("Could not find content's parent tag")
        elif len(content) > 1:
            raise RuntimeError("Found more than one 'debut_journal' tag")

        # Fill paragraphs with all ps inside the divs that has the content
        debut_journal = content[0].parent
        
        next_div = debut_journal.find_next_sibling("div", class_=None)

        while next_div is not None:
            paragraphs.extend(next_div.find_all('p', recursive=False))
            next_div = next_div.find_next_sibling("div", class_=None)

    dialogs = ProcessParagraphs(paragraphs)
    return dialogs
 



def ExtractDialogs(content):
    """ Entry point of ExtractDialogs with a text object containing the content of an html page
    """
    soup = BeautifulSoup(content, 'lxml')
    #soup = BeautifulSoup(content.encode('utf-8'), 'lxml')

    return __ExtractDialogs__(soup)
