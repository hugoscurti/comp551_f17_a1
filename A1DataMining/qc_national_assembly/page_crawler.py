import codecs
import requests
import pickle
import os
import sys

# User-defined imports
import searchresult_crawler as crawler
import extract_dialogs as exdiag

# Change default encoding
reload(sys)
sys.setdefaultencoding('UTF-8')

assembly_folder = "./qc_national_assembly/raw_data/assembly"
committees_folder = "./qc_national_assembly/raw_data/committees/"
merged_folder = "./qc_national_assembly/raw_data/merged/"

result_dump_folder = "./qc_national_assembly/dialogs/"
all_dialogs_file = os.path.join(result_dump_folder, "all_diags")
link_mapping_file = os.path.join(result_dump_folder, "link_mapping")

all_links_file = "./qc_national_assembly/all_links"

dialog_filename = "dialogs_{0}"

# base url is http://www.assnat.qc.ca/en/travaux-parlementaires/journaux-debats.html
server_url = "http://www.assnat.qc.ca"


##### MAIN #####
def main():

    # 1. Get all links for possible dialog html pages from Quebec's national assembly
    if os.path.isfile(all_links_file):
        with open(all_links_file, 'rb') as f:
            all_links = pickle.load(f)

    else:
        all_links = crawler.get_links(merged_folder, server_url)


        # Then dump in file
        with open(all_links_file, 'wb') as f:
            pickle.dump(all_links, f)

    if os.path.isfile(link_mapping_file):
        with open(link_mapping_file, 'rb') as f:
            link_mapping = pickle.load(f)
    else:
        ids = {}
        next_id = 1
        # Get a mapping of all links
        for link in all_links:
            if ids.has_key(link):
                print "Link in double : ", link
            ids[link] = next_id
            next_id += 1

        link_mapping = {'ids':ids, 'next_id':next_id}
        # Store the info about mappings
        with open(link_mapping_file, 'wb') as f:
            pickle.dump(link_mapping, f)
        

    for link in all_links:

        # Get dump file associated with this link
        if (link_mapping['ids'].has_key(link)):
            link_filename = os.path.join(result_dump_folder, dialog_filename.format(link_mapping['ids'][link]))
        else:
            link_filename = os.path.join(result_dump_folder, dialog_filename.format(link_mapping['next_id']))
            link_mapping['next_id'] = link_mapping['next_id'] + 1
            with open(link_mapping_file, 'wb') as f:
                pickle.dump(link_mapping, f)

        if os.path.isfile(link_filename):
            # We already have the dump file for this link, go to next
            # print "Already extracted, id : ", link_mapping['ids'][link], ", link : ", link
            continue

        r = requests.get(link)
        if r.text is not None:

            # Extract dialog from this page
            try:
                diag = exdiag.ExtractDialogs(r.text)
            except:
                diag = None
                print "Error while extracting dialog...", sys.exc_info()[0]
                pass

            if diag is not None and len(diag) > 0:
                # Dump the file in it's file
                with open(link_filename, 'wb') as f:
                    pickle.dump(diag, f)
            else:
                print "No dialog, link : " , link
        else:
            print "Req. response empty, link : ", link


# Entry to main
if __name__ == "__main__":
    main()